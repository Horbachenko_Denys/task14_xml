<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
     <xsl:template match="/">
         <html>
             <body syle="font-family: Arial; font-size: 12pt; background-color: #EEE">
                 <div style="background-color: green; color: black;">
                     <h1>Banks</h1>
                 </div>
                 <table border="1">
                     <tr bgcolor="#9acd32">
                         <th>name</th>
                         <th>registrationCountry</th>
                         <th>DepositorName</th>
                         <th>id</th>
                         <th>Type</th>
                         <th>Amount</th>
                         <th>Probability</th>
                         <th>TimeConstraints</th>
                     </tr>
                         <xsl:for-each select="banks/bank">
                             <tr>
                                 <td><xsl:value-of select="name"/></td>
                                 <td><xsl:value-of select="registrationCountry"/></td>
                                 <td><xsl:value-of select="DepositorName"/></td>
                                 <td><xsl:value-of select="id"/></td>
                                 <td><xsl:value-of select="Type"/></td>
                                 <td><xsl:value-of select="Amount"/></td>
                                 <td><xsl:value-of select="Probability"/></td>
                                 <td><xsl:value-of select="TimeConstraints"/></td>
                             </tr>
                         </xsl:for-each>
                 </table>
             </body>
         </html>
     </xsl:template>
</xsl:stylesheet>
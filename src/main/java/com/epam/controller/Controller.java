package com.epam.controller;

public interface Controller {

    void parseXml();

    void generateHtml();

    void generateNewXml();
}




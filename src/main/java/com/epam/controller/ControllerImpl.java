package com.epam.controller;


import com.epam.model.Model;
import com.epam.model.ModelImpl;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }

    @Override
    public void parseXml() {
        model.parseXml();
    }

    @Override
    public void generateHtml() {
        model.generateHtml();
    }

    @Override
    public void generateNewXml() {
        model.generateNewXml();
    }
}
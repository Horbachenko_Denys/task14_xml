package com.epam.model.parser.sax;

import com.epam.model.Bank;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;



public class SaxHandler extends DefaultHandler {
    private List<Bank> userList = new ArrayList<>();
    private Stack elementStack = new Stack();
    private Stack objectStack = new Stack();

    public void startDocument() {
        System.out.println("===========SAX===========");
    }

    public void endDocument() {
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        this.elementStack.push(qName);
        if("bank".equals(qName)) {
            Bank bank = new Bank();
            if (attributes != null && attributes.getLength() == 1) {
                bank.setName(attributes.getValue(0));
            }
            this.objectStack.push(bank);
        }
    }

    public void endElement(String uri, String localName, String qName) {
        this.elementStack.pop();
        if ("bank".equals(qName)) {
            Bank object = (Bank) this.objectStack.pop();
            this.userList.add(object);
        }
    }

    public void characters (char[] ch, int start, int length) {
        String value = new String(ch,start,length).trim();

        if (value.length() == 0) {
            return;
        }
        if ("name".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setName(value);
        }
        if ("registrationCountry".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setRegistrationCountry(value);
        }
        if ("DepositorName".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setDepositorName(value);
        }
        if ("id".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setId(Integer.parseInt(value));
        }
        if ("Type".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setType(value);
        }
        if ("Amount".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setAmount(Integer.parseInt(value));
        }
        if ("Probability".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setProbability(Double.parseDouble(value));
        }
        if ("TimeConstraints".equals(currentElement())) {
            Bank bank = (Bank) this.objectStack.peek();
            bank.setTime(Integer.parseInt(value));
        }
    }

    private String currentElement() {
        return this.elementStack.peek().toString();
    }

    public List<Bank> getUsers() {
        return userList;
    }
}

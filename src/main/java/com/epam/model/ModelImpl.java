package com.epam.model;


import com.epam.model.parser.dom.DomParser;
import com.epam.model.parser.sax.SaxHandler;
import com.epam.model.parser.sax.SaxParser;
import com.epam.model.parser.stax.StAXParser;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ModelImpl implements Model {

    SaxHandler saxHandler = new SaxHandler();
    List<Bank> bankList = new ArrayList<>(saxHandler.getUsers());

    @Override
    public void parseXml() {
        DomParser domParser = new DomParser();
        domParser.startDomParser();
        SaxParser parser = new SaxParser();
        try {
            parser.startSaxParser();
        StAXParser staxParser = new StAXParser();
        staxParser.startStaxParser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void generateHtml() {
        try {
            File fileXml = new File("E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task14_xml\\src\\main\\resources\\xml\\banks");
            File fileXsl = new File("E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task14_xml\\src\\main\\resources\\xml\\banks.xsl");
            File fileHtml = new File("E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task14_xml\\src\\main\\resources\\xml\\banks.html");

            TransformerFactory tFactory = TransformerFactory.newInstance();

            Source xslDoc = new StreamSource(fileXsl);

            Source xmlDoc = new StreamSource(fileXml);

            OutputStream htmlFile = new FileOutputStream(fileHtml);

            Transformer transform = tFactory.newTransformer(xslDoc);

            transform.transform(xmlDoc, new StreamResult(htmlFile));

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    @Override
    public void generateNewXml() {
        ConvertToXml convertToXml = new ConvertToXml();
        try {
            convertToXml.convertIntoXML(bankList);
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }
    }
}
package com.epam.model;

public class Bank {

    private String name;
    private String registrationCountry;
    private String depositorName;
    private int id;
    private String type;
    private int amount;
    private double probability;
    private int time;

    public void setName(String name) {
        this.name = name;
    }

    public void setRegistrationCountry(String registrationCountry) {
        this.registrationCountry = registrationCountry;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public String getRegistrationCountry() {
        return registrationCountry;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public int getAmount() {
        return amount;
    }

    public double getProbability() {
        return probability;
    }

    public int getTime() {
        return time;
    }

    @Override
    public String toString() {
       return "Bank name: " +  this.name + ", registration: " +  this.registrationCountry + ", depositor: " +
               this.depositorName + " Id: " +  this.id + ", type: " +  this.type + ", amount: " +  this.amount + "$" +
               ", probability: " +  this.probability + ", time: " +  this.time + " years" + "\n" ;
    }
}


package com.epam.model;


public interface Model {

    void parseXml();

    void generateHtml();

    void generateNewXml();

}
